import random as rn
import math
import matplotlib.pyplot as plt


def next_base(is_cg):
    if is_cg:
        cg_switch = rn.randint(1, 100) <= 10  # 10% chance to switch to normal cg-amount
    else:
        cg_switch = rn.randint(1, 100) <= 2  # 2% chance to switch to high cg-amount
    if not is_cg and not cg_switch or is_cg and cg_switch:
        return ["A+", "C+", "G+", "T+"][rn.randint(0, 3)]  # 25% chance for each base
    else:
        is_ac = rn.randint(1, 100) <= 25  # 25% chance to be either A or C
        if is_ac:
            return ["A-", "T-"][rn.randint(0, 1)]
        else:
            return ["G-", "C-"][rn.randint(0, 1)]


def create_seq(a, l):
    seq = ""
    path = []
    # check if path starts in not_is_cg
    if rn.randint(1, 100) <= a["B"]["-"] * 100:
        cg_symbol = "-"
    else:
        cg_symbol = "+"
    tmp_rn = rn.randint(1, 100)
    if tmp_rn <= a["A"][cg_symbol] * 100:
        seq = "A"
    elif tmp_rn <= (a["A"][cg_symbol] + a["T"][cg_symbol]) * 100:
        seq = "T"
    elif tmp_rn <= (a["A"][cg_symbol] + a["T"][cg_symbol] + a["G"][cg_symbol]) * 100:
        seq = "G"
    elif tmp_rn <= (a["A"][cg_symbol] + a["T"][cg_symbol] + a["G"][cg_symbol] + a["C"][cg_symbol]) * 100:
        seq = "C"
    path.append(seq + cg_symbol)

    for i in range(l - 1):
        base = next_base(path[i][1] == "+")
        seq += base[0]
        path.append(base)

    return seq, path


def viterbi(seq, e, a):
    """Takes a symbol sequence, dict of emission chances and dict of transmission chances. Returns a tuple with most likely path and its logarithmic probability"""
    a = {k1: {k_v_pairs[0]: math.log2(k_v_pairs[1]) for k_v_pairs in a[k1].items()} for k1 in a.keys()}
    e = {k1: {k_v_pairs[0]: math.log2(k_v_pairs[1]) if k_v_pairs[1] != 0 else -math.inf for k_v_pairs in e[k1].items()} for k1 in e.keys()}
    n = len(seq)
    states = list(e.keys())  # possible states in the path
    m = len(states)

    M = [[[-math.inf, None] for i in range(n + 1)] for j in range(m + 1)]  # when NOT using log

    M[0][0][0] = 0  # Startzustand

    for t in range(n):
        tmp_symbol = seq[t]

        for z in range(m):
            max_val = -math.inf
            max_pre = None
            tmp_state = states[z]

            for k in range(m):
                emission_chance_log = e[tmp_state][tmp_symbol]

                if t == 0:
                    transition_chance_log = a[tmp_state[0]][tmp_state[1]]
                else:
                    symbol_trans = a[tmp_state[0]][tmp_state[1]]
                    hidden_trans = a[states[k][1]][tmp_state[1]]

                    transition_chance_log = symbol_trans + hidden_trans

                predecessor_chance = a["B"][tmp_state[1]] if t == 0 else M[k + 1][t][0]  # make sure Startzustand is used when in first column

                tmp_val = emission_chance_log + transition_chance_log + predecessor_chance
                tmp_pre = (0, 0) if t == 0 else (k + 1, t)

                if tmp_val > max_val:
                    max_val = tmp_val
                    max_pre = tmp_pre

            M[z + 1][t + 1] = [max_val, max_pre]

    max_idx = 0
    max_res = -math.inf
    for k in range(m):
        if M[k + 1][n][0] > max_res:
            max_idx = k + 1
            max_res = M[k + 1][n][0]

    return M[max_idx][n][0], traceback(M, max_idx, e)


def traceback(M, max_idx, e):
    n = len(M[0]) - 1
    v_path = []

    while n != 0:
        current = M[max_idx][n]
        print(M[max_idx][n])
        predecessor = current[1]
        state = list(e.keys())[max_idx - 1]
        v_path = [(state)] + v_path
        # print(current[0])
        max_idx = predecessor[0]
        n -= 1

    return v_path


def test_viterbi(e, a, seqs, paths):
    viterbi_paths = []
    viterbi_paths_chance = []
    for seq in seqs:
        tmp = viterbi(seq, e, a)
        viterbi_paths.append(tmp[1])
        viterbi_paths_chance.append(tmp[0])
    return viterbi_paths


def create_data(a, l, amount):
    seqs, paths = [], []
    for i in range(amount):
        seq, path = create_seq(a, l)
        seqs.append(seq)
        paths.append(path)
    return seqs, paths


def calc_sensitivity(path, v_path):
    tps = 0
    fns = 0
    for el, other in zip(path, v_path):
        if el[1] == "+" and other[1] == "+":
            tps += 1
        elif el[1] == "+" and other[1] == "-":
            fns += 1
    return tps / (tps + fns)


def calc_specificity(path, v_path):
    tns = 0
    fps = 0
    for el, other in zip(path, v_path):
        if el[1] == "-" and other[1] == "-":
            tns += 1
        elif el[1] == "-" and other[1] == "+":
            fps += 1
    return tns / (tns + fps)


def print_path(v_path):
    just_states = [s[1] for s in v_path]
    formatted_list = [s + "\n" if i % 200 == 0 else s for i, s in enumerate(just_states)]
    formatted_list[0] = formatted_list[0][0]
    formatted = "".join(formatted_list)
    print(formatted)
    return formatted


def main():
    a = {
        "A": {"-": 0.25, "+": 0.125},
        "T": {"-": 0.25, "+": 0.125},
        "G": {"-": 0.25, "+": 0.375},
        "C": {"-": 0.25, "+": 0.375},
        "-": {"-": 0.98, "+": 0.02},
        "+": {"-": 0.1, "+": 0.9},
        "B": {"-": 0.5, "+": 0.5}
    }
    # Nguyen
    # a = {
    #     "A": {"-": 0.25, "+": 0.125},
    #     "T": {"-": 0.25, "+": 0.125},
    #     "G": {"-": 0.25, "+": 0.375},
    #     "C": {"-": 0.25, "+": 0.375},
    #     "-": {"-": 0.4, "+": 0.6},
    #     "+": {"-": 0.1, "+": 0.9},
    #     "B": {"-": 0.3, "+": 0.7}
    # }
    # Riegelmann
    # a = {
    #     "A": {"-": 0.25, "+": 0.125},
    #     "T": {"-": 0.25, "+": 0.125},
    #     "G": {"-": 0.25, "+": 0.375},
    #     "C": {"-": 0.25, "+": 0.375},
    #     "-": {"-": 0.98, "+": 0.02},
    #     "+": {"-": 0.01, "+": 0.99},
    #     "B": {"-": 0.5, "+": 0.5}
    # }
    # Elsen1
    # a = {
    #     "A": {"-": 0.25, "+": 0.125},
    #     "T": {"-": 0.25, "+": 0.125},
    #     "G": {"-": 0.25, "+": 0.375},
    #     "C": {"-": 0.25, "+": 0.375},
    #     "-": {"-": 0.9, "+": 0.1},
    #     "+": {"-": 0.02, "+": 0.98},
    #     "B": {"-": 0.5, "+": 0.5}
    # }
    # higher sens same spec, doesnt work anymore :(
    # a = {
    #     "A": {"-": 0.25, "+": 0.125},
    #     "T": {"-": 0.25, "+": 0.125},
    #     "G": {"-": 0.25, "+": 0.375},
    #     "C": {"-": 0.25, "+": 0.375},
    #     "-": {"-": 0.9, "+": 0.1},
    #     "+": {"-": 0.02, "+": 0.98},
    #     "B": {"-": 0.5, "+": 0.5}
    # }
    # 0 sens 1 spec
    # a = {
    #     "A": {"-": 0.25, "+": 0.25},
    #     "T": {"-": 0.25, "+": 0.25},
    #     "G": {"-": 0.25, "+": 0.25},
    #     "C": {"-": 0.25, "+": 0.25},
    #     "-": {"-": 0.98, "+": 0.02},
    #     "+": {"-": 0.1, "+": 0.9},
    #     "B": {"-": 0.5, "+": 0.5}
    # }

    # sens=spec
    # dallas
    # a = {
    #     "A": {"-": 0.3, "+": 0.2},
    #     "T": {"-": 0.3, "+": 0.2},
    #     "G": {"-": 0.2, "+": 0.3},
    #     "C": {"-": 0.2, "+": 0.3},
    #     "-": {"-": 0.6, "+": 0.4},
    #     "+": {"-": 0.5, "+": 0.5},
    #     "B": {"-": 0.5, "+": 0.5}
    # }
    # e = {
    #     e: {b : 1 if b ==e[0] else 0 for b in "ACTG"} for e in [c+s for c in "ACTG" for s in "-+"]
    # }

    e = {'A-': {'A': 1, 'C': 0, 'T': 0, 'G': 0}, 'A+': {'A': 1, 'C': 0, 'T': 0, 'G': 0},
         'C-': {'A': 0, 'C': 1, 'T': 0, 'G': 0}, 'C+': {'A': 0, 'C': 1, 'T': 0, 'G': 0},
         'T-': {'A': 0, 'C': 0, 'T': 1, 'G': 0}, 'T+': {'A': 0, 'C': 0, 'T': 1, 'G': 0},
         'G-': {'A': 0, 'C': 0, 'T': 0, 'G': 1}, 'G+': {'A': 0, 'C': 0, 'T': 0, 'G': 1}}
    # test viterbi
    # print(viterbi("A",e,a))
    # print(viterbi("G", e, a))
    # print(viterbi("T", e, a))
    # print(viterbi("C", e, a))
    # print(viterbi("ATGC", e, a))
    # print(viterbi("GGTC",e,a))
    # print(viterbi("TACTAGCTACATCGCGCTACACGCATCATCG",e,a))
    # print(viterbi("ATGC"*10,e,a))
    # v_path = viterbi("GGCACTGAA",e,a)   # dallas
    # pair = create_seq(a,1000)
    # print("Symbols:\t{}\nPath:\t\t{}\n".format(pair[0],"".join(pair[1])))
    v_path = viterbi("GGTCGCCCTCTGCGGGCCGCGGCGCGAACAGTAGCGCCGGCGCGATTGTAGCCTACGCGCGGGCGTCCGTCGACTTGAGCCAATACCGGCCTTGGCTTTGGGGGTCGGGTCCACCCAGCGGCGGGCGGAGGTGGGGCACCCAGTCCGCTCTAAAGAGAAAGACGCGCGGCGCGGCGACCCACGAGGCCATGGCCGTACTGGGAGGCGGACGCGGTCCGGGCCGCCCGCACGAGGGGCCGGGGGCTCGTCGACAGGCCCTCGGCCGACAAGCTGGTCGGGCCTCGGCACCGGCGTCAGTCGGTGATTTCACCTGCGTGCGACCTCTGCTGGGCCAAGATGCTCTCGCGACGGGCGCCCCCATTCCGCGCTGCTGGCTGGGGACGTTGGTCCGAGGAGCCCGGTAACCTATCCCATCTCTCTCAGGCGGACGTCCCCTTCGCCCACCCGGATCGGCGGGCACGCCGGGCCGTCCTTCCAGCAGGATTCAGCTCGGATGGGCGCGTACGCGCAAACGCGCTGGGGCGGCCGTAACCCCTGGCCCGCTCCGCCCCTGGTCTAATCCTTCACCGTATGGCTCGGGATGCCCGGTGGGGCCGCCTCGAGGCTCCAGACGGGAACGCACGAACGTCCCAGCTGGCCCGGATTCACGGCGGACGACAGAGGTTCGACCTTGGAGCTACAAGTGTCCGGCGCTCTCGGCCGCAGGCGAACTCCGCCAACCGATTTCGTGGGCGCCTGACGGAAGCTGCAATCTCCAGGGACGGGCGGTAGCGAGCGTGAACGGGAATCTTGCGTCGGTCGGGGACTGTGTCGGGCCGCCAGGGTCCGCGGTCGCGCCACGCTCGGCGCGCCCGCTCGCCGCCGGGGGAAGCCCGGCACTGCTGCCCGTCGGGCGCACGGCCTCCACCGTGGGCTAGGCTGGCGTGGATGGGAGCACGACGTGAGTCAAGGCCTCGGGTCGGGCTTTGTTGGTTTCCCGAGTGTGGCCGTACCTACGGGG", e, a)
    # v_path = viterbi("CGGGCGCCCTCCTCGCCCACCCGGATCGGCGGGCCCGCCGGGACG", e, a)
    # v_path = viterbi("GGCAC", e, a)
    # v_path = viterbi("GGGGGGG", e, a)
    # v_path = viterbi("ATGC"*10,e,a)
    # v_path = viterbi("GAGATTT", e, a)
    print(v_path)
    print_path(v_path[1])

    # test sensitivity and specificity calculations
    # print(calc_specificity(["G+"], ["G+"]))
    # print(calc_specificity(["G+","G+"], ["G+","G-"]))
    #
    # print(calc_sensitivity(["G-"], ["G-"]))
    # print(calc_sensitivity(["G-","G-"],["G-","G+"]))

    # a3
    # length, amount = 300, 300
    # seqs, paths = create_data(a, length, amount)
    # v_paths = test_viterbi(e, a, seqs, paths)
    # sensitivities = []
    # specificities = []
    #
    # for path, v_path in zip(paths, v_paths):
    #     sensitivities.append(calc_sensitivity(path, v_path))
    #     specificities.append(calc_specificity(path,v_path))
    #
    # plt.boxplot([sensitivities,specificities])
    # plt.title("Length: {} Amount: {} \nA: {} T: {}\nG: {} C: {}\n-: {} +: {}".format(length,amount,a["A"],a["T"],a["G"],a["C"],a["-"],a["+"]), fontdict={'fontsize' : 8})
    # plt.ylim((0, 1))
    # plt.xticks([1,2],["Sensitivity", "Specificity"])
    # # plt.savefig("higher_sens_same_spec.png",format="png")
    # plt.show()
    # for i in range(amount):
    #     print("Symbols:\t{}\nPath\t\t{}\nViterbi:\t{}\n\n".format("".join(map(lambda c: c+" ",seqs[i])),"".join(paths[i]),v_paths[i]))


main()
